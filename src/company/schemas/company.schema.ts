import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
// import * as mongoose from 'mongoose';
// import { Address } from './address.schema';

export type CompanyDocument = HydratedDocument<Company>;

@Schema()
export class Company {
  @Prop({ unique: true, required: true })
  email: string;

  @Prop({ required: true })
  name: string;

  //   @Prop({ type: mongoose.Schema.Types.ObjectId, ref: Address.name })
  //   address: Address;
}

export const CompanySchema = SchemaFactory.createForClass(Company);
