import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { CompanyService } from './company.service';
import { CreateOneCompanyDto } from './dto/create-company.dto';
import { GetCompaniesFilterDto } from './dto/get-company.dto';
import { Company } from './schemas/company.schema';

@Controller('company')
export class CompanyController {
  constructor(private companyService: CompanyService) {}

  @Get()
  getCompanies(
    @Query() filters: GetCompaniesFilterDto,
  ): Promise<Array<Company>> {
    // if request have no filter sent, then GET all transaction records
    if (Object.keys(filters).length) {
      //   return this.transactionService.getTransactionsByFilters(filters);
      // TODO
    } else {
      return this.companyService.getAllCompanies();
    }
  }

  @Post()
  async createOneCompany(
    @Body() createOneCompanyDto: CreateOneCompanyDto,
  ): Promise<Company> {
    return this.companyService.createOneCompany(createOneCompanyDto);
  }

  @Delete('/:id')
  async deleteCompanyById(@Param('id') id: string) {
    return this.companyService.deleteCompanyById(id);
  }

  @Get('/:id')
  async getCompanyById(@Param('id') id: string): Promise<Company> {
    return this.companyService.getCompanyById(id);
  }
}
