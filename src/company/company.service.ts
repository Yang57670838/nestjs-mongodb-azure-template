import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Company } from './schemas/company.schema';
import { CreateOneCompanyDto } from './dto/create-company.dto';

@Injectable()
export class CompanyService {
  constructor(
    @InjectModel(Company.name) private companyModel: Model<Company>,
  ) {}

  async createOneCompany(
    createOneCompanyDto: CreateOneCompanyDto,
  ): Promise<Company> {
    const { name, email } = createOneCompanyDto;
    const newCompany: Company = {
      name,
      email,
    };
    const newRecord = new this.companyModel(newCompany);
    await newRecord.save();
    return newRecord.toObject({ versionKey: false });
  }

  async deleteCompanyById(id: string) {
    return this.companyModel.deleteOne({ _id: id });
  }

  async getAllCompanies(): Promise<Array<Company>> {
    return this.companyModel.find();
  }

  async getCompanyById(id: string): Promise<Company> {
    if (Types.ObjectId.isValid(id)) {
      const found = await this.companyModel.findById(id);
      if (!found) {
        throw new NotFoundException(`Transaction with ID ${id} not found`);
      }
      return found;
    }
    throw new BadRequestException('Please provide correct id');
  }
}
