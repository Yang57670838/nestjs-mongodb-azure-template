import { IsOptional, IsString } from 'class-validator';

// find list of companies based on filter
export class GetCompaniesFilterDto {
  @IsOptional()
  @IsString()
  name?: string;

  @IsString({ always: false })
  email?: string;
}
