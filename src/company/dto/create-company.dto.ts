import { IsNotEmpty, IsEmail, IsString } from 'class-validator';

export class CreateOneCompanyDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  @IsEmail()
  email: string;

  //   @IsNotEmpty()
  //   @IsString()
  //   city: string;

  //   @IsNotEmpty()
  //   @IsString()
  //   street: string;
}
