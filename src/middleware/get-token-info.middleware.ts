import { Injectable, NestMiddleware, Logger } from '@nestjs/common';
import { Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class GetTokenInfoMiddleware implements NestMiddleware {
  private logger = new Logger('GetTokenInfoMiddleware', { timestamp: true });

  use(req: Request, res: Response, next: () => void) {
    const jwtToken = req.headers.authorization;
    if (!jwtToken) {
      // use guard service to validate auth, so here just pass to next middleware/routes in the chain
      next();
      return;
    }
    try {
      // decode jwt
      const userPayload = jwt.verify(jwtToken, process.env.JWT_SECRET);
      if (userPayload) {
        this.logger.verbose(
          `User details in token: ${JSON.stringify(userPayload)}`,
        );
        req['userPayload'] = userPayload;
      }
    } catch (err) {
      this.logger.error(`verify token failing`, err.stack);
    }
    next();
  }
}
