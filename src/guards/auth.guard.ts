import {
  Injectable,
  CanActivate,
  ExecutionContext,
  UnauthorizedException,
  Logger,
} from '@nestjs/common';

@Injectable()
export class AuthGuard implements CanActivate {
  private logger = new Logger('AuthGuard', { timestamp: true });

  canActivate(context: ExecutionContext): boolean {
    const host = context.switchToHttp();
    const request = host.getRequest();

    // check if userPayload is pass from middleware which decode client token there
    const userPayload = request['userPayload'];
    if (!userPayload) {
      this.logger.error('Denying access. User is not authenticated.');
      throw new UnauthorizedException();
    }
    this.logger.log('Allowing access. User is authenticated.');
    return true;
  }
}
