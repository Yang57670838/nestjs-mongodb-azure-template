import {
  Injectable,
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Logger,
} from '@nestjs/common';

@Injectable()
export class AuthorizationGuard implements CanActivate {
  private logger = new Logger('AuthGuard', { timestamp: true });

  constructor(private allowedRoles: string[]) {}

  canActivate(context: ExecutionContext): boolean {
    const host = context.switchToHttp();
    const request = host.getRequest();

    // userPayload is pass from middleware which decode client token there
    const userPayload = request['userPayload'];
    // auth.guard already check if userPayload is undefined or not in controller level
    // here just assume token always verified, so we will use this gaurd in the endpoint level to check verified user permission..
    const allowed = this.isAllowed(userPayload.role);

    if (!allowed) {
      this.logger.error(
        'User is authenticated but not authorized, denaying the endpoint access',
      );
      throw new ForbiddenException();
    }
    this.logger.log('User is authorized, allowing endpoint access');
    return true;
  }

  isAllowed(userRole: string) {
    this.logger.verbose(
      `Comparing roles: ${this.allowedRoles}, user role is ${userRole}`,
    );
    let allowed = false;
    if (this.allowedRoles.includes(userRole)) {
      allowed = true;
    }
    return allowed;
  }
}
