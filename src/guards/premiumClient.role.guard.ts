import { Injectable } from '@nestjs/common';
import { AuthorizationGuard } from './role.guard';

@Injectable()
export class PremiumClientGuard extends AuthorizationGuard {
  constructor() {
    // alllow client with PREMIUM role
    super(['PREMIUM']);
  }
}
