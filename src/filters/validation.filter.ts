import { ExceptionFilter, ArgumentsHost, Catch, Logger } from '@nestjs/common';
import { Request, Response } from 'express';
import { ValidationException } from './validation.exception';

@Catch(ValidationException)
export class ValidationFilter implements ExceptionFilter {
  private logger = new Logger('HttpExceptionFilter', { timestamp: true });

  catch(exception: ValidationException, host: ArgumentsHost) {
    // TODO: consider logs into db for track purpose
    this.logger.error(
      `fallback exception ${JSON.stringify(exception)} from host ${host}`,
    );

    const ctx = host.switchToHttp(); //http or rpc or web socket

    // get current context of http request, like request and response..
    const request = ctx.getRequest<Request>();
    const response = ctx.getResponse<Response>();

    // define the response we will send back to client
    return response.status(400).json({
      statusCode: 400,
      createdBy: 'ValidationFilter',
      validationErrors: exception.validationErrors,
      timestamp: new Date().toISOString(),
      path: request.url,
    });
  }
}
