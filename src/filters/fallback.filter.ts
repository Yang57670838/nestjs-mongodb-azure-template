import {
  ExceptionFilter,
  HttpException,
  ArgumentsHost,
  Catch,
  Logger,
} from '@nestjs/common';
import { Request, Response } from 'express';

@Catch()
export class FallbackExceptionFilter implements ExceptionFilter {
  private logger = new Logger('FallbackExceptionFilter', { timestamp: true });

  catch(exception: HttpException, host: ArgumentsHost) {
    // TODO: consider logs into db for track purpose
    this.logger.error(
      `fallback exception ${JSON.stringify(exception)} from host ${host}`,
    );

    const ctx = host.switchToHttp(); //http or rpc or web socket

    // get current context of http request, like request and response..
    const request = ctx.getRequest<Request>();
    const response = ctx.getResponse<Response>();

    // define the response we will send back to client
    return response.status(500).json({
      statusCode: 500,
      createdBy: 'FallbackExceptionFilter',
      errorMessage: exception.message || 'Unexcepted error ocurred',
      timestamp: new Date().toISOString(),
      path: request.url,
    });
  }
}
