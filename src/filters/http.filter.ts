import {
  ExceptionFilter,
  HttpException,
  ArgumentsHost,
  Catch,
  Logger,
} from '@nestjs/common';
import { Request, Response } from 'express';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  private logger = new Logger('HttpExceptionFilter', { timestamp: true });

  catch(exception: HttpException, host: ArgumentsHost) {
    // TODO: consider logs into db for track purpose
    this.logger.error(
      `fallback exception ${JSON.stringify(exception)} from host ${host}`,
    );

    const ctx = host.switchToHttp(); //http or rpc or web socket

    // get current context of http request, like request and response..
    const request = ctx.getRequest<Request>();
    const response = ctx.getResponse<Response>();

    // get current defined exception we throw (NotFoundException for example)
    const status = exception.getStatus();

    // define the response we will send back to client
    return response.status(status).json({
      status: status,
      createdBy: 'HttpExceptionFilter',
      errorMessage: exception.message,
      timestamp: new Date().toISOString(),
      path: request.url,
    });
  }
}
