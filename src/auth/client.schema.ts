import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type ClientDocument = HydratedDocument<Client>;

@Schema()
export class Client {
  @Prop({ unique: true, required: true })
  email: string;

  // support single role for each client only for now..
  @Prop()
  role: string; // client can be NORMAL, PREMIUM, etc..
}

export const ClientSchema = SchemaFactory.createForClass(Client);
