import {
  Body,
  Controller,
  Post,
  Res,
  UnauthorizedException,
  Logger,
} from '@nestjs/common';
import { Response } from 'express';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Client } from './client.schema';
import * as jwt from 'jsonwebtoken';

@Controller('login')
export class AuthController {
  private logger = new Logger('AuthController', { timestamp: true });

  constructor(@InjectModel(Client.name) private clientModel: Model<Client>) {}

  @Post()
  async login(@Body('email') email: string, @Res() response: Response) {
    const client = await this.clientModel.findOne({ email });
    // if client not existing in db
    if (!client) {
      this.logger.error('Wrong Credential');
      throw new UnauthorizedException();
    }
    // TODO: validate by ping together email as well, move it to service layer
    const authJwtToken = jwt.sign(
      {
        email,
        role: 'NORMAL', // TODO: get role from database
      },
      process.env.JWT_SECRET,
    );
    // send JWT back to client
    return response.json({ token: authJwtToken });
  }
}
