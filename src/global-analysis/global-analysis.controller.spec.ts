import { Test, TestingModule } from '@nestjs/testing';
import { GlobalAnalysisController } from './global-analysis.controller';

describe('GlobalAnalysisController', () => {
  let controller: GlobalAnalysisController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GlobalAnalysisController],
    }).compile();

    controller = module.get<GlobalAnalysisController>(GlobalAnalysisController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
