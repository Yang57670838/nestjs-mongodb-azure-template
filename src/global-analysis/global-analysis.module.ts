import { Module } from '@nestjs/common';
import { GlobalAnalysisController } from './global-analysis.controller';

@Module({
  controllers: [GlobalAnalysisController]
})
export class GlobalAnalysisModule {}
