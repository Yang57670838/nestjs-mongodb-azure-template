import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '../guards/auth.guard';
import { PremiumClientGuard } from '../guards/premiumClient.role.guard';

@Controller('global-analysis')
@UseGuards(AuthGuard)
export class GlobalAnalysisController {
  @Get()
  @UseGuards(PremiumClientGuard)
  getAll() {
    return `This action returns the analysis result payload`;
  }
}
