import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { PropertyImage } from './property-image.schema';
import { AddOnePropertyImageDto } from './dto/add-one-property-image.dto';

// since its simple db behaviour, we dont create a separate repository layer from this business service layer
@Injectable()
export class PropertyImageService {
  constructor(
    @InjectModel(PropertyImage.name)
    private propertyImageModel: Model<PropertyImage>,
  ) {}

  async createOnePropertyImage(
    addOnePropertyImageDto: AddOnePropertyImageDto,
  ): Promise<PropertyImage> {
    const { title, url } = addOnePropertyImageDto;
    const newPropertyImage: PropertyImage = {
      title,
      url,
    };
    // const newRecord = new this.propertyImageModel(newPropertyImage);
    // await newRecord.save();
    // return newRecord.toObject({ versionKey: false });
    // use .create() instead .save() for easier unit test
    const res = await this.propertyImageModel.create(newPropertyImage);
    return res;
  }

  // TODO: change to get all images belong to one property only!
  async getAllPropertyImages(): Promise<Array<PropertyImage>> {
    return this.propertyImageModel.find();
  }
}
