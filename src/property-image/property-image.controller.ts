import { Controller, Get, Post, Body, UseGuards } from '@nestjs/common';
import { PropertyImageService } from './property-image.service';
import { AddOnePropertyImageDto } from './dto/add-one-property-image.dto';
import { PropertyImage } from './property-image.schema';
import { AuthGuard } from '../guards/auth.guard';

@Controller('property-image')
@UseGuards(AuthGuard)
export class PropertyImageController {
  constructor(private propertyImageService: PropertyImageService) {}

  @Post()
  async addOnePropertyImage(
    @Body() addOnePropertyImageDto: AddOnePropertyImageDto,
  ): Promise<PropertyImage> {
    return this.propertyImageService.createOnePropertyImage(
      addOnePropertyImageDto,
    );
  }

  @Get()
  getAllPripertyImages(): Promise<Array<PropertyImage>> {
    return this.propertyImageService.getAllPropertyImages();
  }
}
