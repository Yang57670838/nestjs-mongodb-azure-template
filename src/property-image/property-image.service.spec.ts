import { Test, TestingModule } from '@nestjs/testing';
import { PropertyImageService } from './property-image.service';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PropertyImage } from './property-image.schema';
import { AddOnePropertyImageDto } from './dto/add-one-property-image.dto';

describe('PropertyImageService', () => {
  let service: PropertyImageService;
  let model: Model<PropertyImage>;
  const mockPropertyImageModel = {
    find: jest.fn(),
    create: jest.fn(),
  };
  const mockOnePropertyImage = {
    _id: '123355',
    title: 'image title',
    url: 'http://url.com',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PropertyImageService,
        {
          provide: getModelToken(PropertyImage.name),
          useValue: mockPropertyImageModel,
        },
      ],
    }).compile();
    service = module.get<PropertyImageService>(PropertyImageService);
    model = module.get<Model<PropertyImage>>(getModelToken(PropertyImage.name));
  });

  describe('getAllPropertyImages', () => {
    it('should return all property images in the database', async () => {
      jest.spyOn(model, 'find').mockResolvedValue([mockOnePropertyImage]);
      const result = await service.getAllPropertyImages();
      expect(result.length).toEqual(1);
      expect(result[0].url).toEqual(mockOnePropertyImage.url);
      expect(model.find).toHaveBeenCalled();
    });
  });

  describe('createOnePropertyImage', () => {
    it('shoukd create and return the new created property image', async () => {
      const newPropertyImageForCreate = {
        title: 'image title',
        url: 'http://url.com',
      };
      jest
        .spyOn(model, 'create')
        .mockImplementationOnce(() =>
          Promise.resolve(newPropertyImageForCreate as any),
        );

      const result = await service.createOnePropertyImage(
        newPropertyImageForCreate as AddOnePropertyImageDto,
      );
      expect(result).toEqual(newPropertyImageForCreate);
    });
  });
});
