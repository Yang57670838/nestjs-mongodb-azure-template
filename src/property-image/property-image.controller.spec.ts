import { Test, TestingModule } from '@nestjs/testing';
import { CanActivate } from '@nestjs/common';
import { PropertyImageController } from './property-image.controller';
import { PropertyImageService } from './property-image.service';
import { PropertyImage } from './property-image.schema';
import { AddOnePropertyImageDto } from './dto/add-one-property-image.dto';

describe('PropertyImageController', () => {
  let controller: PropertyImageController;
  const mockService = {
    createOnePropertyImage: jest.fn(),
    getAllPropertyImages: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PropertyImageController],
      providers: [
        {
          provide: PropertyImageService,
          useValue: mockService,
        },
      ],
    }).compile();
    controller = module.get<PropertyImageController>(PropertyImageController);
  });

  it('POST addOnePropertyImage => should create a new property image by a given data', async () => {
    const addPropertyImageDto = {
      title: 'title',
      url: 'url',
    } as AddOnePropertyImageDto;

    const createdPropertyImage = {
      title: 'title',
      url: 'url',
    } as PropertyImage;

    jest
      .spyOn(mockService, 'createOnePropertyImage')
      .mockReturnValue(createdPropertyImage);
    const result = await controller.addOnePropertyImage(addPropertyImageDto);
    expect(mockService.createOnePropertyImage).toBeCalled();
    expect(mockService.createOnePropertyImage).toBeCalledWith(
      addPropertyImageDto,
    );
    expect(result).toEqual(createdPropertyImage);
  });

  it('GET getAllPripertyImages => should return an array of property images', async () => {
    const createdPropertyImage = {
      title: 'title',
      url: 'url',
    } as PropertyImage;

    const imageList = [createdPropertyImage];
    jest.spyOn(mockService, 'getAllPropertyImages').mockReturnValue(imageList);
    const result = await controller.getAllPripertyImages();
    expect(result).toEqual(imageList);
    expect(mockService.getAllPropertyImages).toBeCalled();
  });
});
