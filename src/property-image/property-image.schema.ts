import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
// import * as mongoose from 'mongoose';

export type PropertyImageDocument = HydratedDocument<PropertyImage>;

@Schema()
export class PropertyImage {
  @Prop({ required: true })
  title: string;

  @Prop({ unique: true, required: true })
  url: string;

  // TODO, ref to property schema
  //   @Prop({
  //     type: mongoose.Schema.Types.ObjectId,
  //     ref: Property.name,
  //     required: true,
  //   })
  //   property: string;
}

export const PropertyImageSchema = SchemaFactory.createForClass(PropertyImage);
