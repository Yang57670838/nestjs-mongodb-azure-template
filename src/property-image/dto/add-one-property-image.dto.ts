import { IsNotEmpty, IsString } from 'class-validator';

export class AddOnePropertyImageDto {
  @IsNotEmpty()
  @IsString()
  title: string;

  @IsNotEmpty()
  @IsString()
  url: string;
}
