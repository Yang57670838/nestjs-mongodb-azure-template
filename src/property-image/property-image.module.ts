import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PropertyImageController } from './property-image.controller';
import { PropertyImage, PropertyImageSchema } from './property-image.schema';
import { PropertyImageService } from './property-image.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: PropertyImage.name, schema: PropertyImageSchema },
    ]),
  ],
  controllers: [PropertyImageController],
  providers: [PropertyImageService],
})
export class PropertyImageModule {}
