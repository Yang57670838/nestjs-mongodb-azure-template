import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Query,
  Put,
} from '@nestjs/common';
import { TransactionService } from './transaction.service';
import { TransactionType } from './transaction.model';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { GetTransactionsFilterDto } from './dto/get-transaction-filter.dto';
import { Transaction } from './schemas/transaction.schema';
import { UpdateTransactionDto } from './dto/update-transaction.dto';

@Controller('transaction')
export class TransactionController {
  constructor(private transactionService: TransactionService) {}

  @Get()
  getTransactions(
    @Query() filters: GetTransactionsFilterDto,
  ): Promise<Array<Transaction>> {
    return this.transactionService.getTransactionsByFilters(filters);
  }

  @Get('/:id')
  async getTransactionById(@Param('id') id: string): Promise<Transaction> {
    return this.transactionService.getTransactionById(id);
  }

  @Post()
  async createTransaction(
    @Body() createTransactionDto: CreateTransactionDto,
  ): Promise<Transaction> {
    if (createTransactionDto.type === TransactionType.EXPENSE) {
      return this.transactionService.createExpenseTransaction(
        createTransactionDto,
      );
    }
    // TODO: else TransactionType
  }

  @Delete('/:id')
  async deleteTransactionById(@Param('id') id: string) {
    return this.transactionService.deleteTransactionById(id);
  }

  @Put('/:id')
  async updateTransactionById(
    @Param('id') id: string,
    @Body() updateTransactionDto: UpdateTransactionDto,
  ): Promise<Transaction> {
    return this.transactionService.updateTransactionById(
      id,
      updateTransactionDto,
    );
  }
}
