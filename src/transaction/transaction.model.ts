export interface TransactionRecord {
  id: string;
  datetime: string;
  amount: string;
  companyId: string;
  type: TransactionType;
  expenseType?: SupportedExpenseType;
  expensePercentage?: number; // default to 100, can be 90, 50, 30 for example
  shortDescription?: string; // for example: accounting fee, team lunch, etc..
  file?: string; // url in storage like s3
  fileName?: string;
  fileFormat?: string;
}

export enum TransactionType {
  EXPENSE = 'EXPENSE',
  EARN = 'EARN',
  REFUND = 'REFUND',
}

export enum SupportedExpenseType {
  WORK_RELATED = 'WORK_RELATED',
  PERSONAL_CREDIT_CARD = 'PERSONAL_CREDIT_CARD',
  EMPLOYEE_SALARY = 'EMPLOYEE_SALARY',
  PETTY_CASH = 'PETTY_CASH',
}
