import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Decimal128, HydratedDocument, Types } from 'mongoose';
import * as mongoose from 'mongoose';
import { Company } from '../../company/schemas/company.schema';

export type TransactionDocument = HydratedDocument<Transaction>;

@Schema({
  toJSON: {
    getters: true,
  },
})
export class Transaction {
  @Prop()
  expensePercentage?: number; // default to 100, can be 90, 50, 30 for example

  @Prop({ required: true })
  datetime: string;

  @Prop({
    required: true,
    type: Types.Decimal128,
    get: (v: Types.Decimal128) => `${v.toString()}`,
  })
  amount: Decimal128;

  @Prop({
    set: (content?: string) => {
      if (content) {
        return content.trim();
      }
      return content;
    },
  })
  shortDescription?: string; // for example: accounting fee, team lunch, etc.. UI must limit input length to short length, so can easily display..

  @Prop({ required: true })
  type: string; // EXPENSE, EARN, REFUND, etc..

  @Prop()
  expenseType?: string; // WORK_RELATED, PERSONAL_CREDIT_CARD, etc..

  @Prop()
  file?: string; // url in storage like s3

  @Prop()
  fileName?: string;

  @Prop()
  fileFormat?: string;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: Company.name,
    required: true,
  })
  company: string;
}

export const TransactionSchema = SchemaFactory.createForClass(Transaction);
