import {
  IsOptional,
  IsDateString,
  IsEnum,
  IsString,
  MaxLength,
  IsNotEmpty,
  IsNumber,
} from 'class-validator';
import { TransactionType, SupportedExpenseType } from '../transaction.model';

// find list of transactions based on filter
// pageNumber&pageSize support pagination to improve performance..
export class GetTransactionsFilterDto {
  @IsNotEmpty()
  @IsString()
  companyId: string;

  @IsNotEmpty()
  @IsString()
  pageNumber: number;

  @IsNotEmpty()
  @IsString()
  pageSize: number;

  @IsOptional()
  @IsDateString()
  dateFrom?: string;

  @IsOptional()
  @IsDateString()
  dateTo?: string;

  @IsOptional()
  @IsString()
  amountFrom?: string;

  @IsOptional()
  @IsString()
  amountTo?: string;

  @IsOptional()
  @IsEnum(TransactionType)
  transactionType?: TransactionType;

  @IsOptional()
  @IsEnum(SupportedExpenseType)
  expenseType?: SupportedExpenseType;

  @IsOptional()
  @IsString()
  @MaxLength(30)
  shortDescriptionSearchString?: string;
}
