import {
  IsNotEmpty,
  IsOptional,
  IsString,
  IsEnum,
  IsNumber,
  MaxLength,
} from 'class-validator';
import { SupportedExpenseType, TransactionType } from '../transaction.model';

export class CreateTransactionDto {
  @IsNotEmpty()
  @IsString()
  amount: string;

  @IsOptional()
  @IsEnum(SupportedExpenseType)
  expenseType: SupportedExpenseType;

  @IsNotEmpty()
  @IsEnum(TransactionType)
  type: TransactionType;

  @IsOptional()
  @IsNumber()
  percentage?: number;

  @IsOptional()
  @IsString()
  @MaxLength(100)
  shortDescription?: string;

  @IsOptional()
  @IsString()
  file?: string;

  @IsOptional()
  @IsString()
  fileName?: string;

  @IsOptional()
  @IsString()
  fileFormat?: string;

  @IsNotEmpty()
  @IsString()
  companyId: string;
}
