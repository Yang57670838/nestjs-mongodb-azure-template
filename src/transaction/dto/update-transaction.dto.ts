import {
  IsOptional,
  IsString,
  IsEnum,
  IsNumber,
  MaxLength,
} from 'class-validator';
import { SupportedExpenseType } from '../transaction.model';

export class UpdateTransactionDto {
  @IsOptional()
  @IsString()
  amount: string;

  @IsOptional()
  @IsEnum(SupportedExpenseType)
  expenseType: SupportedExpenseType;

  @IsOptional()
  @IsNumber()
  percentage?: number;

  @IsOptional()
  @IsString()
  @MaxLength(100)
  shortDescription?: string;

  @IsOptional()
  @IsString()
  file?: string;

  @IsOptional()
  @IsString()
  fileName?: string;

  @IsOptional()
  @IsString()
  fileFormat?: string;
}
