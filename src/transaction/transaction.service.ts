import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { TransactionType } from './transaction.model';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { GetTransactionsFilterDto } from './dto/get-transaction-filter.dto';
import { Transaction } from './schemas/transaction.schema';

@Injectable()
export class TransactionService {
  constructor(
    @InjectModel(Transaction.name) private transactionModel: Model<Transaction>,
  ) {}

  // should never happens..
  // async getAllTransactions(): Promise<Array<Transaction>> {
  //   return this.transactionModel.find();
  // }

  async getTransactionsByFilters(
    filters: GetTransactionsFilterDto,
  ): Promise<Array<Transaction>> {
    const {
      dateFrom,
      dateTo,
      amountFrom,
      amountTo,
      companyId,
      transactionType,
      expenseType,
      shortDescriptionSearchString,
      pageNumber,
      pageSize,
    } = filters;
    // TODO: complete the filter
    console.log('dateFrom', dateFrom);
    console.log('dateTo', dateTo);
    console.log('amountFrom', amountFrom);
    console.log('amountTo', amountTo);
    console.log('companyId', companyId);
    console.log('transactionType', transactionType);
    console.log('expenseType', expenseType);
    console.log('shortDescriptionSearchString', shortDescriptionSearchString);
    console.log('pageNumber', pageNumber);
    console.log('pageSize', pageSize);
    // TODO: fix query type
    let query: any = {
      company: companyId,
    };
    if (expenseType) {
      query = {
        ...query,
        expenseType,
      };
    }
    if (shortDescriptionSearchString) {
      query = {
        ...query,
        shortDescription: {
          $regex: shortDescriptionSearchString,
          $options: 'i',
        },
      };
    }
    if (amountFrom) {
      query = {
        ...query,
        amount: { $gte: new Types.Decimal128(amountFrom) },
      };
    }
    if (amountTo) {
      query = {
        ...query,
        amount: { $lte: new Types.Decimal128(amountTo) },
      };
    }
    return this.transactionModel.find(query, null, {
      skip: Number(pageNumber) * Number(pageSize),
      limit: Number(pageSize),
      sort: {
        datetime: -1, // sort by datetime descending by default..
      },
    });
  }

  async getTransactionById(id: string): Promise<Transaction> {
    if (Types.ObjectId.isValid(id)) {
      const found = await this.transactionModel.findById(id);
      if (!found) {
        throw new NotFoundException(`Transaction with ID ${id} not found`);
      }
      return found;
    }
    throw new BadRequestException('Something bad happened');
  }

  async createExpenseTransaction(
    createTransactionDto: CreateTransactionDto,
  ): Promise<Transaction> {
    const { amount, expenseType, percentage, shortDescription, companyId } =
      createTransactionDto;
    const newExpenseRecord: Transaction = {
      datetime: new Date().toISOString(), // TODO: frontend can give us the transaction utc datetime..
      amount: new Types.Decimal128(amount) as any, // TODO: fix type..
      type: TransactionType.EXPENSE,
      expenseType: expenseType,
      expensePercentage: percentage || 100,
      shortDescription: shortDescription || null,
      company: companyId,
    };
    const newTransaction = new this.transactionModel(newExpenseRecord);
    await newTransaction.save();
    return newTransaction.toObject({ versionKey: false });
  }

  deleteTransactionById(id: string) {
    return this.transactionModel.deleteOne({ _id: id });
  }

  updateTransactionById(
    id: string,
    updateTransactionDto: UpdateTransactionDto,
  ): Promise<Transaction> {
    return this.transactionModel.findOneAndUpdate(
      { _id: id },
      {
        ...updateTransactionDto,
        amount: new Types.Decimal128(updateTransactionDto.amount) as any, // TODO: fix type..
      },
      { new: true },
    );
  }
}
