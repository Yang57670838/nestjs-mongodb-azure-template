import { Module, MiddlewareConsumer } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TransactionModule } from './transaction/transaction.module';
import { CompanyModule } from './company/company.module';
import { AuthModule } from './auth/auth.module';
import { GetTokenInfoMiddleware } from './middleware/get-token-info.middleware';
import { CompanyController } from './company/company.controller';
import { TransactionController } from './transaction/transaction.controller';
import { PropertyImageModule } from './property-image/property-image.module';
import { GlobalAnalysisModule } from './global-analysis/global-analysis.module';
import { PropertyImageController } from './property-image/property-image.controller';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [`.env.db.${process.env.NODE_ENV}`, '.env.base'],
      isGlobal: true,
    }),
    TransactionModule,
    MongooseModule.forRoot(process.env.DATABASE_CONNECTION_STRING, {
      dbName: process.env.DATABASE_NAME,
      // readPreference: 'secondaryPreferred' // TODO: add this once database upgrade to replacs
    }),
    CompanyModule,
    AuthModule,
    PropertyImageModule,
    GlobalAnalysisModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(GetTokenInfoMiddleware)
      .forRoutes(
        CompanyController,
        TransactionController,
        PropertyImageController,
      );
  }
}
