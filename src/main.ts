import { ValidationPipe, ValidationError, Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './filters/http.filter';
import { FallbackExceptionFilter } from './filters/fallback.filter';
import { ValidationFilter } from './filters/validation.filter';
import { ValidationException } from './filters/validation.exception';

async function bootstrap() {
  const logger = new Logger();
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (errors: Array<ValidationError>) => {
        const messages = errors.map(
          (e) =>
            `${e.property} has wrong value ${e.value}, ${Object.values(
              e.constraints,
            ).join(', ')} `,
        );
        return new ValidationException(messages);
      },
    }),
  );
  app.setGlobalPrefix(process.env.GLOBAL_PREFIX);
  app.useGlobalFilters(
    new FallbackExceptionFilter(),
    new HttpExceptionFilter(),
    new ValidationFilter(),
  );
  const port = process.env.PORT ? parseInt(process.env.PORT) : 8080;
  await app.listen(port, '0.0.0.0'); // default to 0.0.0.0 though, 0.0.0.0 is required for cloud run
  logger.log(`App listening on port ${port}`);
}
bootstrap();
