node 20

## different environments

### production build from Mac M CPU
run nextjs container in GCP cloud run, connect to remote database <br />
```
docker buildx build --platform linux/amd64 -t nestjs/bff:alpha . 
docker tag <imageId> australia-southeast2-docker.pkg.dev/<projectId>/<repoName>/nestjsbff:alpha
gcloud auth configure-docker australia-southeast2-docker.pkg.dev
docker push <imageName>:alpha
```

### local test production build
run production built image in local, connect to mongo Atlas <br />

```
docker build -t nestjs/bff:alpha . 
docker tag <imageId> australia-southeast2-docker.pkg.dev/<projectId>/<repoName>/nestjsbff:alpha
gcloud auth configure-docker australia-southeast2-docker.pkg.dev
docker push <imageName>:alpha
docker run --rm -p 8080:8080 <image_id>
```

### local
run nextjs container and mongodb container in docker compose, for test nestjs container is working or not <br />

```
docker-compose --verbose up --build
docker compose down -v
```

### development
run nextjs directly without container, connect to mongodb container in docker compose, for fast development purpose <br />
First comment out bff service on docker compose <br />

```
yarn start:dev
docker-compose -f docker-compose.development.yml up -d
docker compose down -v
```


## TODO
1, cloud run for staging is only allocate CPU when request come (like a serverless)
but in production, will change to always allocate CPU
set min instances to 0 for staging, set min instances to 1 for production

2, first step: change to only allow UI from fire base auth can access this app, this service will verify firebase auth token sent by UI

3, later in production, allow traffic to this through VPC only, in the beginning through public

4, for twilio webhook endpoint, add calculate amount of receiving sms message in database


## TODO for network
1, setup vpc for cloud run, using vpc connector, assign vpc static ip address to mongoDB altas white list..

## long term TODO
4, if dataset grow too large, not middle size, or too many write, then switch from replicas pattern to sharded database