
FROM node:20-alpine AS build
WORKDIR /usr/src/app
COPY --chown=node:node package.json yarn.lock ./
# install devDependencies first for nestjs/cli
RUN yarn install --frozen-lockfile
COPY --chown=node:node . .
RUN yarn run build
RUN yarn install --frozen-lockfile --production && yarn cache clean --force
FROM node:20-alpine
COPY --chown=node:node --from=build /usr/src/app/node_modules ./node_modules
COPY --chown=node:node --from=build /usr/src/app/dist ./dist
COPY --chown=node:node --from=build /usr/src/app/.env.base ./
COPY --chown=node:node --from=build /usr/src/app/.env.db.production ./
USER node

ENV NODE_ENV=production

CMD ["node", "dist/main.js"]